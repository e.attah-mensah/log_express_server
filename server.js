const express = require('express');
const app = express();
const port = 3000;

const db = require('./database.js');

app.use(express.json());

app.get('/', (req, res) => {
  res.send('Hello World!');
});

// get list of log entries
app.get('/api/log_entries', (req, res) => {
  let sql = `select * from log_entry`;
  db.all(sql, [], (err, rows) => {
    if (err) {
      res.status(400).json({
        "error": err.message
      });
    }
    res.json({
      "message": "success",
      "data": rows
    });
  });
});

// POST a new log entry
app.post('/api/log_entry/', (req, res) => {
  let data = {
    remote_addr: req.body.remote_addr,
    remote_addr_city: req.body.remote_addr_city,
    remote_addr_country: req.body.remote_addr_country,
    remote_addr_continent: req.body.remote_addr_continent,
    remote_user: req.body.remote_user,
    time_local: req.body.time_local,
    request: req.body.request,
    request_http_version: req.body.request_http_version,
    request_method: req.body.request_method,
    request_path: req.body.request_path,
    request_query: req.body.request_query,
    status: req.body.status,
    bytes_sent: req.body.bytes_sent,
    http_referer: req.body.http_referer,
    http_user_agent: req.body.http_user_agent,
    scheme: req.body.scheme,
    http_accept_language: req.body.http_accept_language,
    http_host: req.body.http_host,
    http_x_client_id: req.body.http_x_client_id,
    server: req.body.server,
    webserver: req.body.webserver,
    remote_logname: req.body.remote_logname,
    http_x_forwarded_for_location: req.body.http_x_forwarded_for_location,
    http_x_forwarded_for: req.body.http_x_forwarded_for,
  };

  let sql = `INSERT INTO log_entry (${Object.keys(data).join(', ')}) VALUES (${Array(24).fill('?').join()})`;
  let params = Object.values(data);

  db.run(sql, params, function (err, result) {
    if (err){
      res.status(400).json({ "error": err.message })
      return;
    }

    res.json({
      "message": "success",
      "data": data,
      "id": this.lastID
    })
  });
});

app.listen(port, () => {
  console.log(`Log Server listening at http://localhost:${port}`);
});
