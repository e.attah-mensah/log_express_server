const sqlite3 = require('sqlite3').verbose();

const dbPath = '../log_api.db';

let db = new sqlite3.Database(dbPath, (err) => {
  if (err) {
    // cannot open db
    console.error(err.message);
    throw err;
  } else {
    console.log(`Connected to database: ${dbPath}`);
    // create table if it doesn't already exist
    db.run(`CREATE TABLE IF NOT EXISTS log_entry (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      remote_addr text,
      remote_addr_city text,
      remote_addr_country text,
      remote_addr_continent text,
      remote_user text,
      time_local text,
      request text,
      request_http_version text,
      request_method text,
      request_path text,
      request_query text,
      status INTEGER,
      bytes_sent INTEGER,
      http_referer text,
      http_user_agent text,
      scheme text,
      http_accept_language text,
      http_host text,
      http_x_client_id text,
      server text,
      webserver text,
      remote_logname text,
      http_x_forwarded_for_location text,
      http_x_forwarded_for text
    )`);
  }
});

module.exports = db;
